#include <iostream>
#include "../ocean/ocean.h"

//NETWORK
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <thread>
#include <chrono>
#include <string>
#include <signal.h>

#define PORT 3425
using namespace std;

Ocean ocean;
bool isSleeping = false;
const int UPDATE_RATE_MS = 1000;

void handleClient(int socketClient){
    string data("");
    while(1){
        if(!ocean.isMoving && !isSleeping){
            data = to_string(ocean.days) + "\t" + to_string(ocean.preysCount) + "\t" + to_string(ocean.predatorsCount) + "\t";
            if(send(socketClient, data.c_str(), data.size(), 0) >= 0){
                this_thread::sleep_for(chrono::milliseconds(UPDATE_RATE_MS));
            }
            else{
                break;
            }
        }
    }
    close(socketClient);
    return;
}

void fserver()
{
    int socketServer = socket(AF_INET, SOCK_STREAM, 0);
    //Avoiding "Bind: the address is already in use." error
    int option = 1;
    setsockopt(socketServer, SOL_SOCKET,(SO_REUSEPORT | SO_REUSEADDR), (char*)&option, sizeof(option));

    struct sockaddr_in socketAddr;
    signal(SIGPIPE, SIG_IGN);

    if(socketServer < 0){
        perror("socket");
        exit(1);
    }

    socketAddr.sin_family = AF_INET;
    socketAddr.sin_port = htons(PORT);
    socketAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(socketServer, (struct sockaddr*) &socketAddr, sizeof(socketAddr)) != 0){
        perror("bind");
        exit(2);
    };
    string data("");
    listen(socketServer, 1);
    int socketClient = -1;
    while(1){
        socketClient = accept(socketServer, NULL, NULL);

        if(socketClient >= 0){
            thread clientThread(handleClient, socketClient);
            clientThread.detach();
        }
        else{
            perror("accept");
            exit(3);
        }
    }
    close(socketServer);
}

int main(int argc, char *argv[])
{
    thread networkThread(fserver);
    networkThread.detach();

    int width, height, preds, preys, stones;
    if(argc == 6){
        width = atoi(argv[1]);
        height = atoi(argv[2]);
        preds = atoi(argv[3]);
        preys = atoi(argv[4]);
        stones = atoi(argv[5]);
    }
    else{
        width = 80;
        height = 50;
        preds = 10;
        preys = 80;
        stones = 10;
    }
    ocean = Ocean(width, height);
    ocean.fillSpaceWith(PREDATOR, preds);
    ocean.fillSpaceWith(PREY, preys);
    ocean.fillSpaceWith(STATIC, stones);

    while(ocean.active.size()){
        ocean.display();
        ocean.makeTurn();
        isSleeping = true;
        this_thread::sleep_for(chrono::milliseconds(UPDATE_RATE_MS));
        isSleeping = false;
        system("clear");
    }
    ocean.display();
    return 0;
}
