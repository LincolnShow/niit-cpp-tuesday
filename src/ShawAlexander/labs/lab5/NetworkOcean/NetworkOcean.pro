TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ../ocean/fishfabric.cpp \
    ../ocean/livingobject.cpp \
    ../ocean/ocean.cpp \
    ../ocean/oceanobject.cpp

SUBDIRS += \
    ../ocean/ocean.pro

DISTFILES += \
    ../ocean/ocean.pro.user \
    ../ocean/ocean.xml

HEADERS += \
    ../ocean/cell.h \
    ../ocean/fishfabric.h \
    ../ocean/livingobject.h \
    ../ocean/ocean.h \
    ../ocean/oceanobject.h \
    ../ocean/settings.h \
    ../ocean/staticobject.h

LIBS += -lpthread
