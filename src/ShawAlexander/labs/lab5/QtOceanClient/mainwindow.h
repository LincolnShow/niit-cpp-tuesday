#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork>
#include <QTimer>
#include <QVector>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void timerAction();
    void socketConnected();
    void socketDisconnected();
    void socketError(QAbstractSocket::SocketError e);

    void on_pb_connect_clicked();

    void on_pb_disconnect_clicked();

private:
    QString IP;
    int updateRate = 0, PORT = 0;
    QVector<double> xdays;
    QVector<double> ypreds;
    QVector<double> ypreys;
    int yLimit = 1000, xLimit = 300;
    QTimer timer;
    QTcpSocket qsocket;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
