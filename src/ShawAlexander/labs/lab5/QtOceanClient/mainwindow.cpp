#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pb_disconnect->setDisabled(true);
    connect(&timer, SIGNAL(timeout()), this, SLOT(timerAction()));
    connect(&qsocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    connect(&qsocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
    //connect(ui->pb_disconnect, SIGNAL(clicked(bool)), this, SLOT(socketDisconnected()));
    ui->plot->addGraph();
    ui->plot->addGraph();
    ui->plot->graph(0)->setPen(QPen(Qt::red));
    ui->plot->graph(1)->setPen(QPen(Qt::green));
    ui->plot->xAxis->setLabel("Days");
    ui->plot->yAxis->setLabel("Fish count");
    ui->plot->xAxis->setRange(0, xLimit);
    ui->plot->yAxis->setRange(0, yLimit);
    //timer.start();
}

MainWindow::~MainWindow()
{
    qsocket.close();
    delete ui;
}

void MainWindow::timerAction()
{
    QString arr(qsocket.readAll());
    QStringList vals;
    if(arr.size()){
        vals = arr.split('\t');
        ui->l_days->setText(vals[0]);
        ui->l_predators->setText(vals[2]);
        ui->l_preys->setText(vals[1]);
        xdays.push_back(vals[0].toDouble());
        ypreys.push_back(vals[1].toDouble());
        ypreds.push_back(vals[2].toDouble());
        ui->plot->graph(0)->setData(xdays, ypreds);
        ui->plot->graph(1)->setData(xdays, ypreys);
        if(vals[0].toInt() >= xLimit){
            xLimit = vals[0].toInt() + (vals[0].toInt()/3);
            ui->plot->xAxis->setRange(0, xLimit);
        }
        if((vals[1].toInt() >= yLimit) | (vals[2].toInt() >= yLimit)){
            yLimit = vals[1].toInt() > vals[2].toInt() ? (vals[1].toInt() + vals[1].toInt()/3) : (vals[2].toInt() + vals[2].toInt()/3);
            ui->plot->yAxis->setRange(0, yLimit);
        }
    }
    ui->plot->replot();
}

void MainWindow::socketConnected()
{
    ui->l_state->setText("CONNECTED");
    ui->pb_connect->setDisabled(true);
    ui->pb_disconnect->setDisabled(false);
    xdays.clear();
    ypreds.clear();
    ypreys.clear();
    qsocket.open(QIODevice::ReadOnly);
    timer.start();
}

void MainWindow::socketDisconnected()
{
    ui->l_state->setText("DISCONNECTED");
    ui->pb_disconnect->setDisabled(true);
    ui->pb_connect->setDisabled(false);
    qsocket.disconnectFromHost();
    qsocket.close();
    timer.stop();
}

void MainWindow::socketError(QAbstractSocket::SocketError e)
{
    ui->l_state->setText("CONNECTION ERROR");
    qsocket.disconnectFromHost();
    qsocket.close();
    timer.stop();
}

void MainWindow::on_pb_connect_clicked()
{
    if(!qsocket.isOpen()){
        ui->l_state->setText("CONNECTING...");
        IP = ui->le_ip->text();
        PORT = ui->le_port->text().toInt();
        updateRate = ui->le_updateRate->text().toInt();
        if(!IP.isEmpty() && updateRate && PORT){
            qsocket.connectToHost(QHostAddress(IP), PORT);
            timer.setInterval(updateRate);
        }
        else{
            ui->l_state->setText("INCORRECT DATA");
        }
    }
}

void MainWindow::on_pb_disconnect_clicked()
{
    socketDisconnected();
}
