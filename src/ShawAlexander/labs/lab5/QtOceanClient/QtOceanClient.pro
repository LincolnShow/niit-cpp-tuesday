#-------------------------------------------------
#
# Project created by QtCreator 2016-05-23T01:03:09
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = QtOceanClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        qcustomplot.cpp

HEADERS  += mainwindow.h \
        qcustomplot.h

FORMS    += mainwindow.ui
INCLUDEPATH += .
