#ifndef CELL_H
#define CELL_H
#include "livingobject.h"


class Cell {
public:
    Cell(){}
    Cell(OceanObject* object){
        cellObject = object;
    }
    ~Cell(){
        delete cellObject;
    }
//protected:
    OceanObject* cellObject = nullptr;
    unsigned short x, y;
};

#endif // CELL_H
