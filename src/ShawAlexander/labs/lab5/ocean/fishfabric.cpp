#include "fishfabric.h"

LivingObject* FishFabric::getNewObject(Roles role)
{
    switch (role) {
    case PREY:
        return new Prey;
    case PREDATOR:
        return new Predator;
    default:
        return nullptr;
    }
}
