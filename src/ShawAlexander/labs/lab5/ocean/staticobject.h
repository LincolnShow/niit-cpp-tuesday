#ifndef STATICOBJECT_H
#define STATICOBJECT_H
#include "oceanobject.h"

class StaticObject : public OceanObject {

public:
    Roles getType(){ return STATIC; }
    StaticObject(){}
};

class Stone : public StaticObject
{
public:
    Stone() {}
};
#endif // STATICOBJECT_H
