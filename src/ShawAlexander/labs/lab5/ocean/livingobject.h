#ifndef LIVINGOBJECT_H
#define LIVINGOBJECT_H
#include "oceanobject.h"
#include <stdlib.h>
class LivingObject : public OceanObject
{
public:
    unsigned short daysAlive = 0, hunger = 0, spawnRate = 0;
    virtual ~LivingObject(){}
    virtual State act() = 0;
};

class Predator : public LivingObject
{
public:
    Roles getType(){ return PREDATOR; }
    Predator();
    virtual State act();
private:

};

class Prey : public LivingObject
{
public:
    Roles getType(){ return PREY; }
    Prey();
    virtual ~Prey(){}
    virtual State act();
};
#endif //LIVINGOBJECT_H
