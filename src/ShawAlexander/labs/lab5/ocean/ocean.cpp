#include "ocean.h"
using namespace std;

Ocean::Ocean(const unsigned short width, const unsigned short height)
{
    for(unsigned long i = 0; i < height; ++i){
        field.push_back(std::vector<Cell>());
        for(unsigned long q = 0; q < width; ++q){
            field[i].push_back(Cell());
            field[i][q].x = q;
            field[i][q].y= i;
        }
    }
    this->width = width;
    this->height = height;
    freeSpace = cellsCount = width * height;
    srand(time(0));
}

void Ocean::fillSpaceWith(Roles role, unsigned short percent)
{
    if(percent > 100)
        percent = 100;
    unsigned long CAP = (cellsCount / 100.0) * percent;
    if(CAP > freeSpace)
        CAP = freeSpace;
    unsigned long added = 0;
    vector<vector<Cell>>::iterator rowIter = field.begin();
    vector<Cell>::iterator columnIter;
    vector<Cell*>freeCells;

    while(rowIter != field.end()){
        columnIter = rowIter->begin();
        while(columnIter != rowIter->end()){
            if(!columnIter->cellObject){
                freeCells.push_back(&*columnIter);
            }
            ++columnIter;
        }
        ++rowIter;
    }
    random_shuffle(freeCells.begin(), freeCells.end());
    for(Cell* c : freeCells){
        if(added == CAP){
            break;
        }
        if(role != STATIC){
            c->cellObject = FishFabric::getNewObject(role);
            active.push_back(c);
        }
        else{
            c->cellObject = new Stone();
        }
        ++added;
    }
    updatePopulation(role, added);
}

void Ocean::makeTurn()
{
    isMoving = true;
    ++days;
    State action;
    list<Cell*>children;
    list<Cell*>::iterator activeIter = active.begin();
    LivingObject* iterObject = (LivingObject*) (*activeIter)->cellObject;
    while(activeIter != active.end()){
        iterObject = (LivingObject*) (*activeIter)->cellObject;
        iterObject->daysAlive++;
        for(unsigned int i = 0; i < (iterObject->getType() == PREY ? PREY_SPEED : PRED_SPEED); ++i){
            action = iterObject->act();
            if(action == DEAD){
                /*string s = (*activeIter)->cellObject->getType() == PREY ?
                            "Prey" : ((*activeIter)->cellObject->getType() == PREDATOR ? "Predator" : "Stone");
                log << s + " died at " +  to_string((*activeIter)->y) << "-" + to_string((*activeIter)->x) << endl;*/

                updatePopulation((*activeIter)->cellObject->getType(), -1);
                (*activeIter)->cellObject->getType() == PREY ? ++preysDied : ++predatorsDied;
                delete (*activeIter)->cellObject;
                (*activeIter)->cellObject = nullptr;
            }
            else if(action == ALIVE){
                Cell* newPos = nullptr;
                if((*activeIter)->cellObject->getType() == PREDATOR){
                    newPos = getFoodNearby(*activeIter);

                    if(newPos){
                        /*string s = (*activeIter)->cellObject->getType() == PREY ?
                                    "Prey" : ((*activeIter)->cellObject->getType() == PREDATOR ? "Predator" : "Stone");
                        log << s + " at " +  to_string((*activeIter)->y) << "-" + to_string((*activeIter)->x) <<
                             " killed prey at " << newPos->y << "-" << newPos->x << endl;*/
                        children.remove(newPos);
                        active.remove(newPos);
                        delete newPos->cellObject;
                        newPos->cellObject = (*activeIter)->cellObject;
                        (*activeIter)->cellObject = nullptr;
                        needSwap = true;
                        swapCell = newPos;

                        if(((LivingObject*)newPos->cellObject)->hunger > 0.0f)
                           ((LivingObject*)newPos->cellObject)->hunger -= PRED_FOODVALUE;
                        updatePopulation(PREY, -1);

                        ((LivingObject*)newPos->cellObject)->spawnRate += PRED_DEPRESSION;
                        ++preysEaten;
                    }

                }

                if(!newPos){
                    newPos = getFreeCell(*activeIter);

                    if(newPos){
                        /*string s = (*activeIter)->cellObject->getType() == PREY ?
                                    "Prey" : ((*activeIter)->cellObject->getType() == PREDATOR ? "Predator" : "Stone");
                        log << s + " moved from " +  to_string((*activeIter)->y) << "-" + to_string((*activeIter)->x) <<
                             " to " << newPos->y << "-" << newPos->x << endl;*/

                        newPos->cellObject = (*activeIter)->cellObject;
                        (*activeIter)->cellObject = nullptr;
                        needSwap = true;
                        swapCell = newPos;

                    }
                    else{
                        /*string s = (*activeIter)->cellObject->getType() == PREY ?
                                    "Prey" : ((*activeIter)->cellObject->getType() == PREDATOR ? "Predator" : "Stone");
                        log << s + " stayed at " +  to_string((*activeIter)->y) << "-" + to_string((*activeIter)->x) << endl;*/
                        break;
                    }
                }

            }
            else if(action == BREED){
                Cell* childPos = getFreeCell(*activeIter);
                if(childPos){
                    /*string s = (*activeIter)->cellObject->getType() == PREY ?
                                "Prey" : ((*activeIter)->cellObject->getType() == PREDATOR ? "Predator" : "Stone");
                    log << s + " at " +  to_string((*activeIter)->y) << "-" + to_string((*activeIter)->x) <<
                         " bred at " << childPos->y << "-" << childPos->x << endl;*/

                    childPos->cellObject = FishFabric::getNewObject((*activeIter)->cellObject->getType());
                    updatePopulation(childPos->cellObject->getType(), 1);
                    children.push_back(childPos);
                }
                else if((*activeIter)->cellObject->getType() == PREDATOR){
                    //--i;
                    if(iterObject->spawnRate - PRED_DEPRESSION >= 0)
                        iterObject->spawnRate -= PRED_DEPRESSION;

                    /*string s = (*activeIter)->cellObject->getType() == PREY ?
                                "Prey" : ((*activeIter)->cellObject->getType() == PREDATOR ? "Predator" : "Stone");
                    log << s + " at " +  to_string((*activeIter)->y) << "-" + to_string((*activeIter)->x) <<
                         " couldn't breed" << endl;*/

                }
                else{
                    break;
                }

            }

            if(!(*activeIter)->cellObject){
                if(needSwap){
                    needSwap = false;
                    *activeIter = swapCell;
                    swapCell = nullptr;
                }
                else{
                    activeIter = active.erase(activeIter);
                    needSwap = true;
                    break;
                }
            }
        }
        if(!needSwap)
            ++activeIter;
        else
            needSwap = false;
    }
    for(Cell* child : children){
        active.push_back(child);
    }
    isMoving = false;
}

void Ocean::display()
{
    vector<vector<Cell>>::iterator rowIter = field.begin();
    vector<Cell>::iterator columnIter;
    while(rowIter != field.end()){
        columnIter = rowIter->begin();
        while(columnIter != rowIter->end()){
            Roles o = FREE;
            if(columnIter->cellObject) {
                o = columnIter->cellObject->getType();
                if(o == PREY){
                    cout << 'o';
                }
                else if(o == PREDATOR){
                    if(((LivingObject*)columnIter->cellObject)->daysAlive >= PRED_ADULTDAY)
                        cout << '*';
                    else
                        cout << '+';
                }
                else
                    cout << '.';
            }
            else{
                cout << ' ';
            }
            ++columnIter;
        }
        cout << endl;
        ++rowIter;
    }
    cout << "\n    TOTAL: " << cellsCount << endl << endl;
    cout << "Predators: " << predatorsCount << "  Predators died: " << predatorsDied << endl;
    cout << "    Preys: " << preysCount << "  Preys died: " << preysDied << "  Preys eaten: " << preysEaten << endl;
    cout << "   Stones: " << stonesCount << endl;
    cout << "     Free: " << freeSpace << endl;
    cout << "   Active: " << active.size() << endl;
    cout << "      Day [" << days << "]" << endl;
}

Cell* Ocean::getFreeCell(Cell *cell)
{
    vector<Cell*>freelist;
    unsigned short x = cell->x, y = cell->y;

    if(x != 0){
        //LEFT
        if(!field[y][x-1].cellObject)
            freelist.push_back(&field[y][x-1]);
        if(y != 0){
            //LEFT_UP
            if(!field[y-1][x-1].cellObject)
                freelist.push_back(&field[y-1][x-1]);
        }
        if(y+1 != height){
            //LEFT_DOWN
            if(!field[y+1][x-1].cellObject)
                freelist.push_back(&field[y+1][x-1]);
        }
    }
    if(x+1 != width){
        //RIGHT
        if(!field[y][x+1].cellObject)
            freelist.push_back(&field[y][x+1]);
        if(y != 0){
            //RIGHT_UP
            if(!field[y-1][x+1].cellObject)
                freelist.push_back(&field[y-1][x+1]);
        }
        if(y+1 != height){
            //RIGHT_DOWN
            if(!field[y+1][x+1].cellObject)
                freelist.push_back(&field[y+1][x+1]);
        }
    }
    if(y != 0){
        //TOP
        if(!field[y-1][x].cellObject)
            freelist.push_back(&field[y-1][x]);
    }
    if(y+1 != height){
        //DOWN
        if(!field[y+1][x].cellObject)
            freelist.push_back(&field[y+1][x]);
    }
    if(freelist.size() != 0){
        return freelist[rand() % freelist.size()];
    }
    else
        return nullptr;
}

Cell* Ocean::getFoodNearby(Cell* cell)
{
    //return nullptr;
    vector<Cell*>preylist;
    unsigned short x = cell->x, y = cell->y;

    if(x != 0){
        //LEFT
        if(field[y][x-1].cellObject && field[y][x-1].cellObject->getType() == PREY)
            preylist.push_back(&field[y][x-1]);
        if(y != 0){
            //LEFT_UP
            if(field[y-1][x-1].cellObject && field[y-1][x-1].cellObject->getType() == PREY)
                preylist.push_back(&field[y-1][x-1]);
        }
        if(y+1 != height){
            //LEFT_DOWN
            if(field[y+1][x-1].cellObject && field[y+1][x-1].cellObject->getType() == PREY)
                preylist.push_back(&field[y+1][x-1]);
        }
    }
    if(x+1 != width){
        //RIGHT
        if(field[y][x+1].cellObject && field[y][x+1].cellObject->getType() == PREY)
            preylist.push_back(&field[y][x+1]);
        if(y != 0){
            //RIGHT_UP
            if(field[y-1][x+1].cellObject && field[y-1][x+1].cellObject->getType() == PREY)
                preylist.push_back(&field[y-1][x+1]);
        }
        if(y+1 != height){
            //RIGHT_DOWN
            if(field[y+1][x+1].cellObject && field[y+1][x+1].cellObject->getType() == PREY)
                preylist.push_back(&field[y+1][x+1]);
        }
    }
    if(y != 0){
        //TOP
        if(field[y-1][x].cellObject && field[y-1][x].cellObject->getType() == PREY)
            preylist.push_back(&field[y-1][x]);
    }
    if(y+1 != height){
        //DOWN
        if(field[y+1][x].cellObject && field[y+1][x].cellObject->getType() == PREY)
            preylist.push_back(&field[y+1][x]);
    }
    if(preylist.size() != 0){
        return preylist[rand() % preylist.size()];
    }
    else
        return nullptr;
}

void Ocean::updatePopulation(Roles role, long long val)
{
    switch(role){
    case STATIC:
        stonesCount += val;
        break;
    case PREY:
        preysCount += val;
        break;
    case PREDATOR:
        predatorsCount += val;
        break;
    default:
        break;
    }
    freeSpace -= val;
}
