#include <iostream>
#include "ocean.h"
#include <unistd.h>
using namespace std;

int main(int argc, char *argv[])
{
    int width, height, preds, preys, stones;
    if(argc == 6){
        width = atoi(argv[1]);
        height = atoi(argv[2]);
        preds = atoi(argv[3]);
        preys = atoi(argv[4]);
        stones = atoi(argv[5]);
    }
    else{
        width = 80;
        height = 50;
        preds = 10;
        preys = 80;
        stones = 10;
    }
    Ocean ocean(width, height);
    ocean.fillSpaceWith(PREDATOR, preds);
    ocean.fillSpaceWith(PREY, preys);
    ocean.fillSpaceWith(STATIC, stones);
    clock_t start;

    double diff = 0.0;
    const int UPDATE_RATE_MS = 250;
    while(ocean.active.size()){
        ocean.display();
        if((diff*100) < UPDATE_RATE_MS){
            usleep(1000* (UPDATE_RATE_MS - (diff*100)));
        }
        start = clock();
        ocean.makeTurn();
        diff = (double) (clock() - start) / CLOCKS_PER_SEC;
        system("clear");
    }
    ocean.display();
    //ocean.log.close();
    return 0;
}
