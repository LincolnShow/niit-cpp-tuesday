#ifndef OCEANOBJECT_H
#define OCEANOBJECT_H
#include "settings.h"

enum Roles{
    FREE, STATIC, PREY, PREDATOR
};
class OceanObject {
public:
    virtual Roles getType() = 0;
    virtual ~OceanObject(){}
};
#endif // OCEANOBJECT_H
