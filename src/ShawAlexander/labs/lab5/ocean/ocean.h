#ifndef OCEAN_H
#define OCEAN_H

//Custom
#include "cell.h"
#include "fishfabric.h"
#include "staticobject.h"
#include "time.h"

//C++
#include <stdlib.h>
#include <vector>
#include <list>
#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <fstream>

class Ocean {
public:
    Ocean(const unsigned short width, const unsigned short height);
    Ocean(){}
    void fillSpaceWith(Roles role, unsigned short percent);
    void makeTurn();
    void display();
    Cell* getFreeCell(Cell* cell);
    Cell* getFoodNearby(Cell* cell);
    void updatePopulation(Roles role, long long val);
//protected:
    std::vector<std::vector<Cell>> field;
    std::list<Cell*> active;
    unsigned long preysCount = 0;
    unsigned long preysDied = 0;
    unsigned long preysEaten = 0;
    unsigned long predatorsCount = 0;
    unsigned long predatorsDied = 0;
    unsigned long predatorsEaten = 0;
    unsigned long stonesCount = 0;
    unsigned long cellsCount = 0;
    unsigned long freeSpace = 0;
    unsigned short width = 0, height = 0;
    unsigned long days = 0;
    bool needSwap = false;
    bool isMoving = false;
    Cell* swapCell = nullptr;

    //std::ofstream log = std::ofstream("/home/alexander/ocean.log");
};

#endif // OCEAN_H
