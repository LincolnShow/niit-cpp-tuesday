#include "livingobject.h"

Prey::Prey()
{
    daysAlive = 0;
    spawnRate = PREY_SPAWNRATE;
}

State Prey::act(){
    if(daysAlive > PREY_LIFETIME){
        return DEAD;
    }
    else{
        if((rand() % 100 + 1) <= spawnRate){
            return BREED;
        }
        return ALIVE;
    }
}

Predator::Predator()
{
    daysAlive = 0;
    hunger = 0;
    spawnRate = PRED_SPAWNRATE;
}

State Predator::act()
{
    hunger += PRED_FOODVALUE;
    if(daysAlive > PRED_LIFETIME || hunger >= 100)
        return DEAD;
    else{
        if(daysAlive >= PRED_ADULTDAY && (rand() % 100 + 1) <= spawnRate){
            spawnRate = 0.0f;
            return BREED;
        }
        return ALIVE;
    }
}
