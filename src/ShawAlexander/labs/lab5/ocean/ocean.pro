TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ocean.cpp \
    livingobject.cpp \
    fishfabric.cpp

HEADERS += \
    ocean.h \
    cell.h \
    staticobject.h \
    livingobject.h \
    oceanobject.h \
    settings.h \
    fishfabric.h
