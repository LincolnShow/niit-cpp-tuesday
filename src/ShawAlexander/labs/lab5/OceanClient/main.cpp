#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <chrono>
#include <thread>

#define IP "127.0.0.1"
#define PORT 3425
using namespace std;

int main(int argc, char *argv[])
{
    short penalty = 3;
    const int UPDATE_RATE_MS = (argc >= 2 ? atoi(argv[1]) : 1000);

    int socketClient = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in serverAddr;
    if(socketClient < 0){
        perror("socket");
        exit(1);
    }

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr(IP);
    int i = connect(socketClient, (struct sockaddr*) &serverAddr, sizeof(serverAddr));

    if(!i){
        while(1){
            char buf[1024] = { 0 };
            if(recv(socketClient, buf, 1024, 0)){
                cout << buf << endl;
            }
            else if(penalty)
                --penalty;
            else
                break;

            this_thread::sleep_for(chrono::milliseconds(UPDATE_RATE_MS));
        }
    }
    return 0;
}
