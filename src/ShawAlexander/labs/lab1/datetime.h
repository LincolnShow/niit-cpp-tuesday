#ifndef DATETIME_H
#define DATETIME_H
#include <time.h>
#include <iostream>
#include <string>
#include <cstring>
class DateTime
{
public:
    DateTime();
    DateTime(int day, int month, int year, int hour = 0, int min = 0, int sec = 0);
    DateTime(DateTime& src);
    tm* getToday();
    std::string getStringDate();
    tm* getFuture(int N);
    tm* getPast(int N);
    int getMonth();
    int getMonthDay();
    int getYear();
    int getHour();
    int getMinutes();
    int getSeconds();
    unsigned int getDifference(const DateTime& date2);
private:
    time_t TIME;
};

#endif // DATETIME_H
