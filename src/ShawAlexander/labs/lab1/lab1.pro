#-------------------------------------------------
#
# Project created by QtCreator 2016-03-26T18:52:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab1
TEMPLATE = app

INCLUDEPATH += .
SOURCES += main.cpp\
        mainwindow.cpp \
    circle.cpp \
    datetime.cpp

HEADERS  += mainwindow.h \
    circle.h \
    datetime.h

FORMS    += mainwindow.ui
CONFIG += c++11
