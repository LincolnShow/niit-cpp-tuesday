#ifndef CIRCLE_H
#define CIRCLE_H


class Circle
{
public:
    Circle();
    Circle(double radius);
    void setRadius(double r);
    void setFerence(double f);
    void setArea(double a);
    double getRadius();
    double getFerence();
    double getArea();
private:
    void validate();
    double radius, ference, area;
};

#endif // CIRCLE_H
