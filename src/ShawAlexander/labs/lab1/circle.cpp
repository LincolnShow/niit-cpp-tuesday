#include "circle.h"
#include <cmath>
Circle::Circle()
{
    radius = ference = area = 0;
}
Circle::Circle(double radius){
    setRadius(radius);
}

void Circle::setRadius(double r)
{
    radius = r;
    ference = 2.0 * 3.14159 * radius;
    area = 3.14159 * radius * radius;
    this->validate();
}

void Circle::setFerence(double f)
{
    ference = f;
    radius = f / (2*3.14159);
    area = 3.14159 * radius * radius;
    this->validate();
}

void Circle::setArea(double a)
{
    area = a;
    radius = sqrt(a / 3.14159);
    ference = 2.0 * 3.14159 * radius;
    this->validate();
}

double Circle::getRadius()
{
    return radius;
}

double Circle::getFerence()
{
    return ference;
}

double Circle::getArea()
{
    return area;
}

void Circle::validate()
{
    if(radius < 0.0 || ference < 0.0 || area < 0.0){
        radius = 0.0;
        ference = 0.0;
        area = 0.0;
    }
}
