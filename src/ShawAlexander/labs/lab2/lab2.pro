#-------------------------------------------------
#
# Project created by QtCreator 2016-03-27T13:50:04
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab2
TEMPLATE = app

INCLUDEPATH += .
SOURCES += main.cpp\
        mainwindow.cpp \
        automata.cpp

HEADERS  += mainwindow.h \
        automata.h

FORMS    += mainwindow.ui
CONFIG += c++11
