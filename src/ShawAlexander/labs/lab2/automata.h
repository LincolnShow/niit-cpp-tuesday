#ifndef AUTOMATA_H
#define AUTOMATA_H
#include <fstream>
#include <vector>
#include <ctype.h>
#include <QXmlStreamReader>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
enum STATES{
    OFF, WAIT, ACCEPT, CHECK, COOK
};
class Automata {
public:
    Automata(std::string filePath = NULL);
    void on();
    void off();
    bool coin(const float value);
    std::vector<std::string> *getNames();
    std::vector<float> *getPrices();
    std::string getState();
    std::string getDrinkName(unsigned int index);
    float getBalance();
    bool choice(unsigned int index);
    float cancel();
private:
    float cash;
    std::vector<std::string>menu;
    std::vector<float> prices;
    STATES state;
    void cook();
    bool check(const unsigned int index);
    void finish();
};

#endif // AUTOMATA_H
