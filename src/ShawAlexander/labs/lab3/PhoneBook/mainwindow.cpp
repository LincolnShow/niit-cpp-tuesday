#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    book = new PhoneBook();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::resizeEvent(QResizeEvent *event){
    QMainWindow::resizeEvent(event);
    if(ui->tableBook->horizontalHeader()->count())
        ui->tableBook->horizontalHeader()->setSectionResizeMode(ui->tableBook->horizontalHeader()->count()-1, QHeaderView::Stretch);
}

void MainWindow::refillTable()
{
    ui->tableBook->clearContents();
    ui->tableBook->setRowCount(0);

    QTableWidgetItem* item;
    int i = 0;
    std::map<unsigned int, std::string> storage = book->getPhoneBook();

    for(auto bookEntry = storage.begin(); bookEntry != storage.end(); ++bookEntry, ++i){
        ui->tableBook->insertRow(i);
        item = new QTableWidgetItem(QString(bookEntry->second.c_str()));
        ui->tableBook->setItem(i, 0, item);
        item = new QTableWidgetItem(QString::number(bookEntry->first));
        ui->tableBook->setItem(i, 1, item);
    }
}

void MainWindow::on_pb_load_clicked()
{
    if(book)
        delete book;
    QString path = QFileDialog::getOpenFileName(this, tr("Select a PhoneBook file"), tr(""), tr("TXT (*.txt)"));
    book = new PhoneBook(path.toStdString());
    refillTable();
}

void MainWindow::on_pb_add_clicked()
{
    std::string newName = ui->le_name->text().toStdString();
    unsigned int newPhone = ui->le_phone->text().toUInt();
    if(!newName.empty() && newPhone){
        if(book->insertEntry(newPhone, newName)){
            ui->tableBook->insertRow(ui->tableBook->rowCount());
            ui->tableBook->setItem(ui->tableBook->rowCount()-1, 0, new QTableWidgetItem(ui->le_name->text()));
            ui->tableBook->setItem(ui->tableBook->rowCount()-1, 1, new QTableWidgetItem(ui->le_phone->text()));
        }
        ui->le_name->clear();
        ui->le_phone->clear();
    }
}

void MainWindow::on_pb_remove_clicked()
{
    int row = ui->tableBook->currentRow();
    if(row >= 0 && (book->eraseEntry(ui->tableBook->item(row, 1)->text().toUInt()))){
        ui->tableBook->removeRow(ui->tableBook->currentRow());
        ui->tableBook->setCurrentCell(-1, -1);
    }
}

void MainWindow::on_le_searchVal_textEdited(const QString &arg1)
{
    if(arg1.isEmpty()){
        refillTable();
    }
    else{
        ui->tableBook->clearContents();
        ui->tableBook->setRowCount(0);

        QTableWidgetItem* item;
        int i = 0;
        std::map<unsigned int, std::string> storage;

        if(ui->comboChoice->currentText() == "Name"){
            storage = book->getSurnameNumbers(arg1.toStdString());
        }
        else{
            storage = book->getPhones(arg1.toUInt());
        }

        for(auto bookEntry = storage.begin(); bookEntry != storage.end(); ++bookEntry, ++i){
            ui->tableBook->insertRow(i);
            item = new QTableWidgetItem(QString(bookEntry->second.c_str()));
            ui->tableBook->setItem(i, 0, item);
            item = new QTableWidgetItem(QString::number(bookEntry->first));
            ui->tableBook->setItem(i, 1, item);
        }
    }
}
