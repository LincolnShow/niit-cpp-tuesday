#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <string.h>
#include "PhoneBook.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_pb_load_clicked();

    void on_pb_add_clicked();

    void on_pb_remove_clicked();

    void on_le_searchVal_textEdited(const QString &arg1);

private:
    PhoneBook* book = nullptr;
    void resizeEvent(QResizeEvent *event);
    void refillTable();
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
