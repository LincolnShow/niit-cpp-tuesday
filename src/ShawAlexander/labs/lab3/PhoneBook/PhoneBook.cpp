#include "PhoneBook.h"
using namespace std;

PhoneBook::PhoneBook(string file_path) {
    string h = file_path;
    ifstream datafile(file_path, ifstream::in);
    int n;
    char buf[1024] = { 0 };
    string s;
    while(datafile.good()){
        datafile.getline(buf, sizeof(buf), '\n');
        if((n = atoi(buf))) {
            s = string(buf);
            s = s.substr(s.find_first_of(' ') + 1);
            insertEntry(n, s);
        }
    }
}

bool PhoneBook::insertEntry(unsigned int number, string name) {
    unsigned int before = book.size();
    std::pair<unsigned int, string> pair(number, name);
    book.insert(pair);
    if(book.size() > before)
        return true;
    return false;
}

std::string PhoneBook::findNameOf(unsigned int number) {
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        if(iter->first == number)
            return iter->second;
        ++iter;
    }
    return "";
}

unsigned int PhoneBook::findNumberOf(std::string name) {
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        if(iter->second == name)
            return iter->first;
        ++iter;
    }
    return 0;
}

std::map<unsigned int, std::string> PhoneBook::getSurnameNumbers(std::string surname)
{
    map<unsigned int, string> results;
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        if(iter->second.find(surname) != string::npos)
            results.insert(*iter);
        ++iter;
    }
    return results;
}

std::map<unsigned int, string> PhoneBook::getPhones(unsigned int phone)
{
    map<unsigned int, string> results;
    map<unsigned int, string>::iterator iter = book.begin();
    string textPhone = to_string(phone);
    while(iter != book.end()){
        string iterTextPhone = to_string(iter->first);
        if(iterTextPhone.find(textPhone) != string::npos)
            results.insert(*iter);
        ++iter;
    }
    return results;
}

std::map<unsigned int, std::string> PhoneBook::getPhoneBook() {
    return book;
}

std::list<unsigned int> PhoneBook::getNumbers() {
    map<unsigned int, string>::iterator iter = book.begin();
    list<unsigned int> results;
    while(iter != book.end()){
        results.push_back(iter->first);
        ++iter;
    }
    return results;
}

std::list<std::string> PhoneBook::getNames() {
    list<string> results;
    map<unsigned int, string>::iterator iter = book.begin();
    while(iter != book.end()){
        results.push_back(iter->second);
        ++iter;
    }
    return results;
}

bool PhoneBook::eraseEntry(unsigned int phone)
{
    for(auto iter = book.begin(); iter != book.end(); ++iter){
        if(iter->first == phone){
            book.erase(iter);
            return true;
        }
    }
    return false;
}
