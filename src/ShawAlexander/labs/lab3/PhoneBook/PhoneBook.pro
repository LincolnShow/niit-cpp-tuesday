#-------------------------------------------------
#
# Project created by QtCreator 2016-04-22T23:14:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PhoneBook
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    PhoneBook.cpp
INCLUDEPATH += .
HEADERS  += mainwindow.h \
    PhoneBook.h

FORMS    += mainwindow.ui
CONFIG += c++11
