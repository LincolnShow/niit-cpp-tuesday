#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT\

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    void setAcceptSlot(QWidget* widget, const char* slot);
private slots:
    \
signals:
    addNew
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
