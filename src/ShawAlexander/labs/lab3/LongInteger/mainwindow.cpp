#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->centralWidget->setLayout(ui->horizontalLayout_2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_sum_clicked()
{
    recalculate('+');

}

void MainWindow::recalculate(char c)
{
    n1 = LongInteger(ui->te_n1->toPlainText().toStdString());
    n2 = LongInteger(ui->te_n2->toPlainText().toStdString());
    if(c == '?'){
        if(n1 == n2)
            ui->te_result->setText("N1 == N2");
        else if(n1 < n2)
            ui->te_result->setText("N1 < N2");
        else
            ui->te_result->setText("N1 > N2");
    }
    else{
        switch(c){
        case '+':
            result = n1 + n2;
            break;
        case '-':
            result = n1 - n2;
            break;
        case '*':
            result = n1 * n2;
            break;
        case '/':
            result = n1 / n2;
            break;
        case '%':
            result = n1 % n2;
            break;
        case '^':
            result = n1 ^ n2;
            break;
        default:
            break;
        }
        ui->te_result->setText(result.getValue().c_str());
    }
}

void MainWindow::on_pb_deduct_clicked()
{
    recalculate('-');
}

void MainWindow::on_pb_multiply_clicked()
{
    recalculate('*');
}

void MainWindow::on_pb_divide_clicked()
{
    recalculate('/');
}

void MainWindow::on_pb_mod_clicked()
{
    recalculate('%');
}

void MainWindow::on_pb_pow_clicked()
{
    recalculate('^');
}

void MainWindow::on_pushButton_clicked()
{
    recalculate('?');
}
