#include "longinteger.h"
using namespace std;
static LongInteger RESULT;
LongInteger::LongInteger()
{
    digits = "";
}
LongInteger::LongInteger(string numString){
        unsigned int N = numString.size();
        if(numString.at(0) == '-'){
            negative = true;
            --N;
        }
        reverse(numString.begin(), numString.end());
        digits = string(numString, 0, N);
        for(char &c : digits){
            if(isWiden(c)){
                digits = "";
                return;
            }
        }
        reverse(numString.begin(), numString.end());
}
LongInteger::LongInteger(const LongInteger& src){
    this->negative = src.negative;
    this->digits = src.digits;
}

void LongInteger::change(string numString)
{
    *this = LongInteger(numString);
}

LongInteger::~LongInteger(){
    this->digits.clear();
}
LongInteger LongInteger::operator+(LongInteger& sum){
    //1 + 2
    if(!this->negative && !sum.negative)
        RESULT = add(*this, sum);
    //-1 + (-2)
    else if(this->negative && sum.negative){
        RESULT = add(*this, sum);
        RESULT.negative = true;
    }
    //-1 + 2
    else if(this->negative && !sum.negative){
        RESULT = *this;
        RESULT.negative = false;
        //-1 + 1
        if(RESULT == sum)
            RESULT.digits = "0";
        //-1 + 2
        else if(RESULT < sum){
            RESULT = deduct(sum, RESULT);
        }
        //-2 + 1
        else{
            RESULT = deduct(RESULT, sum);
            RESULT.negative = true;
        }
    }
    // 1 + (-2)
    else {
        RESULT = (sum+*this);
    }
    return RESULT;
}

LongInteger LongInteger::operator++()
{
    string::iterator ch = this->digits.begin();
    *ch += 1;
    while((*ch) > '9'){
        if((ch + 1) == this->digits.end()){
            this->digits += '0';
        }
        *ch = '0';
        *(ch+1) += 1;
        ch++;
    }
    return *this;
}

LongInteger LongInteger::operator++(int)
{
    LongInteger tmp("1");
    RESULT = LongInteger(*this);
    if(this->negative){
        this->negative = false;
        *this -= tmp;
        this->negative = true;
    }
    else
        *this += tmp;
    return RESULT;
}

LongInteger LongInteger::operator+=(LongInteger &sum)
{
    *this = *this + sum;
    return *this;
}
LongInteger LongInteger::operator-(LongInteger& sub){
    //Both positive
    if(!this->negative && !sub.negative){
        //1-2
        if(*this < sub){
            RESULT = deduct(sub, *this);
            RESULT.negative = true;
        }
        //2-1 || 2-2
        else
            RESULT = deduct(*this, sub);

    }
    //-1 - (-2)
    else if(this->negative && sub.negative){
        RESULT = LongInteger(sub);
        RESULT.negative = false;
        RESULT = (*this + RESULT);
    }
    //-1 - 2
    else if(this->negative){
        RESULT = (add(*this, sub));
        RESULT.negative = true;
    }
    //1 - -(2)
    else{
        RESULT = add(*this, sub);
    }
    return RESULT;
}

LongInteger LongInteger::operator--()
{
    LongInteger tmp("1");
    if(this->negative){
        this->negative = false;
        *this += tmp;
        this->negative = true;
    }
    else
        *this -= tmp;
    return *this;
}

LongInteger LongInteger::operator--(int)
{
    LongInteger tmp("1");
    RESULT = LongInteger(*this);
    if(this->negative){
        this->negative = false;
        *this += tmp;
        this->negative = true;
    }
    else
        *this -= tmp;
    return RESULT;
}

LongInteger LongInteger::operator-=(LongInteger &sub)
{
    *this = (*this - sub);
    return *this;
}

bool LongInteger::operator<(LongInteger &lint)
{
    int i = this->digits.size()-1;
    //-1 < -1
    if(this->negative && lint.negative){
        if(this->digits.size() == lint.digits.size()){
            while(i >= 0){
                if(this->digits[i] > lint.digits[i])
                    return true;
                else if(this->digits[i] < lint.digits[i])
                    return false;
                --i;
            }
            return false;
        }
        else
            return this->digits.size() > lint.digits.size()? true : false;
    }
    //-1 < 1
    else if(this->negative && !lint.negative)
        return true;
    //1 < -1
    else if(!this->negative && lint.negative)
        return false;
    //1 < 1
    else {
        long long i = this->digits.size();
        if(this->digits.size() == lint.digits.size()){
            while(i >= 0){
                if(this->digits[i] > lint.digits[i])
                    return false;
                else if(this->digits[i] < lint.digits[i])
                    return true;
                --i;
            }
            return false;
        }
        return this->digits.size() < lint.digits.size() ? true : false;
    }
}

bool LongInteger::operator<=(LongInteger &lint)
{
    return ((*this < lint) || (*this == lint));
}

bool LongInteger::operator>(LongInteger &lint)
{
    if(*this != lint)
        return !(*this < lint);
    return false;
}

bool LongInteger::operator>=(LongInteger &lint)
{
    return ((*this > lint) || (*this == lint));
}

bool LongInteger::operator==(LongInteger &lint)
{
    if(!this->negative == !lint.negative){
        return this->digits == lint.digits;
    }
    return false;
}

bool LongInteger::operator==(const char* snum)
{
    return (this->digits == snum);
}

bool LongInteger::operator!=(LongInteger &lint)
{
    return !(*this==lint);
}

LongInteger LongInteger::pow(LongInteger x, LongInteger n)
{
    static LongInteger two("2");
    if (n == "0")
        RESULT.digits = "1";
    else if (n=="1")
        RESULT = x;
    else if ((n % two) == "0")
        RESULT = pow(x * x, n/two);
    else
        RESULT = pow(x * x, n /two) * x;
    return RESULT;
}

LongInteger LongInteger::add(LongInteger &n1, LongInteger &n2)
{
    LongInteger S_RESULT = n1;
    if(n2.digits.size() >= S_RESULT.digits.size()){
        while(S_RESULT.digits.size() <= n2.digits.size()){
            S_RESULT.digits += '0';
        }
    }
    S_RESULT.digits += '0';
    for(unsigned int i = 0, pos; i < n2.digits.size(); ++i){
        pos = i;
        char sDigit = n2.digits[i] - '0';
        S_RESULT.digits[i] += sDigit;
        char *ch = &S_RESULT.digits[i];
        while(isWiden(*ch) && pos < n1.digits.size()){
            *ch -= 10;
            *(ch + 1) += 1;
            if(!isWiden(*ch)){
                ++ch;
                ++pos;
            }
        }

    }
    S_RESULT.adjust();
    return S_RESULT;
}

LongInteger LongInteger::deduct(LongInteger &n1, LongInteger &n2)
{
    LongInteger D_RESULT = n1;
    if(n1 == n2)
        D_RESULT.digits = "0";
    else{
        for(unsigned int i = 0, pos; i < n2.digits.size(); ++i){
            char sDigit = n2.digits[i] - '0';
            D_RESULT.digits[i] -= sDigit;
            char *ch = &D_RESULT.digits[i];
            pos = i;
            while(isWiden(*ch) && pos < n1.digits.size()){
                *ch += 10;
                *(ch + 1) -= 1;
                if(!isWiden(*ch)){
                    ++ch;
                    ++pos;
                }
            }
        }
    }
    D_RESULT.adjust();
    return D_RESULT;
}

LongInteger LongInteger::operator*(LongInteger& fac)
{
    LongInteger tmp;
    RESULT = LongInteger("0");
    short number = 0;
    //this-number cycle
    for(unsigned int n1_i = 0; n1_i < this->digits.size(); ++n1_i){
        number = this->digits[n1_i] - '0';
        tmp = LongInteger("0");
        tmp.digits[0] -= '0';
        //multiplier cycle
        for(unsigned int fac_i = 0, pos = 0; fac_i < fac.digits.size(); ++fac_i, ++pos){
            if(pos == tmp.digits.size()){
                tmp.digits += '\0';
            }
            char facN = fac.digits[fac_i] - '0';
            tmp.digits[pos] += (number * facN);
            char *ch = &tmp.digits[pos];
            while(isWiden(((*ch) + '0'))){
                if((ch+1) == &(tmp.digits[tmp.digits.size()])){
                    tmp.digits += '\0';
                    ch = &tmp.digits[pos];
                }
                *ch -= 10;
                *(ch + 1) += 1;
            }
        }
        //to normal numbers
        for(unsigned int i = 0; i < tmp.digits.size(); ++i){
            tmp.digits[i] += '0';
        }
        //correcting digits
        if(!(tmp == "0")){
            reverse(tmp.digits.begin(), tmp.digits.end());
            for(unsigned int i = 0; i < n1_i; ++i){
                tmp.digits += '0';
            }
            reverse(tmp.digits.begin(), tmp.digits.end());
        }
        RESULT += tmp;
    }
    if(!this->negative != !fac.negative)
        RESULT.negative = true;
    RESULT.adjust();
    return RESULT;
}

LongInteger LongInteger::operator*=(LongInteger &fac)
{
    *this = (*this * fac);
    return *this;
}

LongInteger LongInteger::operator/(LongInteger &div)
{
    RESULT = *this;
    reverse(RESULT.digits.begin(), RESULT.digits.end());
    LongInteger partQuot;
    LongInteger answer;
    LongInteger divREV = div;
    LongInteger thisREV = *this;
    reverse(thisREV.digits.begin(), thisREV.digits.end());
    reverse(divREV.digits.begin(), divREV.digits.end());
    RESULT.digits = RESULT.digits.substr(0, div.digits.size());
    reverse(RESULT.digits.begin(), RESULT.digits.end());
    if(RESULT >= div)
        partQuot.digits = thisREV.digits.substr(0, div.digits.size());
    else
        partQuot.digits = thisREV.digits.substr(0, div.digits.size()+1);
    reverse(partQuot.digits.begin(), partQuot.digits.end());
    unsigned long long resultSize = (this->digits.size() - partQuot.digits.size()) + 1;
    long long pos = (this->digits.size() - partQuot.digits.size());
    if(pos)
        --pos;
    LongInteger closest;
    LongInteger i;
    LongInteger one("1");
    while(answer.digits.size() < resultSize){
        closest = div;
        i.digits = "2";
        while(closest < partQuot){
            closest = LongInteger(i * div);
            ++i;
        }
        if(closest != partQuot){
            closest = deduct(closest, div);
            i = deduct(i, one);
        }
        partQuot = deduct(partQuot, closest);
        closest.digits == div.digits ? i.digits = "1" : i = deduct(i, one);
        answer.digits += i.digits;
        if(partQuot < div){
            if(partQuot.digits.size() == 1 && partQuot.digits[0] == '0')
                partQuot.digits.clear();
            if(pos >= 0){
                partQuot.digits.insert(partQuot.digits.begin(), this->digits[pos]);
                --pos;
            }
        }
    }
    reverse(answer.digits.begin(), answer.digits.end());
    if(this->negative || div.negative)
        answer.negative = true;
    return answer;
}

LongInteger LongInteger::operator/=(LongInteger &div)
{
    *this = (*this / div);
    return *this;
}

LongInteger LongInteger::operator%(LongInteger &div)
{
    RESULT = *this;
    reverse(RESULT.digits.begin(), RESULT.digits.end());
    LongInteger partQuot;
    LongInteger answer;
    LongInteger divREV = div;
    LongInteger thisREV = *this;
    reverse(thisREV.digits.begin(), thisREV.digits.end());
    reverse(divREV.digits.begin(), divREV.digits.end());
    RESULT.digits = RESULT.digits.substr(0, div.digits.size());
    reverse(RESULT.digits.begin(), RESULT.digits.end());
    if(RESULT >= div)
        partQuot.digits = thisREV.digits.substr(0, div.digits.size());
    else
        partQuot.digits = thisREV.digits.substr(0, div.digits.size()+1);
    reverse(partQuot.digits.begin(), partQuot.digits.end());
    unsigned long long resultSize = (this->digits.size() - partQuot.digits.size()) + 1;
    long long pos = (this->digits.size() - partQuot.digits.size()) - 1;

    LongInteger closest;
    LongInteger i;
    LongInteger one("1");
    while(answer.digits.size() < resultSize){
        closest = div;
        i.digits = "2";
        while(closest < partQuot){
            closest = LongInteger(i * div);
            ++i;
        }
        if(closest != partQuot){
            closest = deduct(closest, div);
            i = deduct(i, one);
        }
        partQuot = deduct(partQuot, closest);
        closest.digits == div.digits ? i.digits = "1" : i = deduct(i, one);
        answer.digits += i.digits;
        if(partQuot < div){
            if(partQuot.digits.size() == 1 && partQuot.digits[0] == '0')
                partQuot.digits.clear();
            if(pos >= 0 && answer.digits.size() < resultSize){
                partQuot.digits.insert(partQuot.digits.begin(), this->digits[pos]);
                --pos;
            }
        }
    }
    if(partQuot.digits.empty())
        partQuot.digits = "0";
    RESULT = partQuot;
    return RESULT;
}

LongInteger LongInteger::operator^(LongInteger &power)
{
    if(power.negative){
        RESULT.digits = "0";
        RESULT.negative = false;
    }
    else{
        RESULT = pow(*this, power);
        if(this->negative)
            RESULT.negative = power.digits[0] % 2 ? true : false;
    }
    return RESULT;
}

void LongInteger::adjust()
{
    while((this->digits[this->digits.size() - 1] == 0)
          || ((this->digits[this->digits.size() - 1] == '0') && this->digits.size() > 1)){
        this->digits.pop_back();
    }
}

bool isWiden(const char& c){
    return !(c >= '0' && c <= '9');
}

string LongInteger::getValue() const{
    string s = digits;
    if(negative)
        s += '-';
    reverse(s.begin(), s.end());
    return s;
}
