#-------------------------------------------------
#
# Project created by QtCreator 2016-04-19T19:45:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LongInteger
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    longinteger.cpp
INCLUDEPATH += .
HEADERS  += mainwindow.h \
    longinteger.h

FORMS    += mainwindow.ui
CONFIG += c++11
