#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "longinteger.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_sum_clicked();

    void on_pb_deduct_clicked();
    void on_pb_multiply_clicked();

    void on_pb_divide_clicked();

    void on_pb_mod_clicked();

    void on_pb_pow_clicked();

    void on_pushButton_clicked();

private:
    LongInteger n1, n2, result;
    Ui::MainWindow *ui;
    void recalculate(char c);
};

#endif // MAINWINDOW_H
