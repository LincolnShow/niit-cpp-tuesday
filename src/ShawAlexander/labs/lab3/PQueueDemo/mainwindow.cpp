#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    rgxV = new QRegExpValidator;
    ui->table_items->verticalHeader()->setVisible(false);
    ui->table_items->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->table_items->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->table_items->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->comboBox->addItem("String");
    ui->comboBox->addItem("Int");
    ui->comboBox->addItem("Char");
    ui->comboBox->addItem("Double");
    connect(ui->pb_delete, SIGNAL(clicked()), this, SLOT(deleteItem()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    ui->le_itemData->clear();
    ui->table_items->clearContents();
    ui->table_items->setRowCount(0);

    QRegExp exp(".*");
    if(arg1 == "Char"){
        stringQ.clear();
        doubleQ.clear();
        intQ.clear();
        exp.setPattern("\\S");
    }
    else if(arg1 == "Double"){
        charQ.clear();
        stringQ.clear();
        intQ.clear();
        exp.setPattern("\\d{,30}\\.\\d{,6}");
    }
    else if(arg1 == "Int"){
        charQ.clear();
        stringQ.clear();
        doubleQ.clear();
        exp.setPattern("-?\\d{,9}");
    }
    else{
        doubleQ.clear();
        charQ.clear();
        intQ.clear();
    }
    rgxV->setRegExp(exp);
    ui->le_itemData->setValidator(rgxV);
}

void MainWindow::on_pb_add_clicked()
{
    if(!ui->le_itemData->text().isEmpty()){
        QTableWidgetItem *val = new QTableWidgetItem(ui->le_itemData->text());
        QTableWidgetItem *priority = new QTableWidgetItem(QString::number(ui->sb_priority->value()));;
        val->setTextAlignment(Qt::AlignHCenter);
        priority->setTextAlignment(Qt::AlignHCenter);

        ui->table_items->insertRow(0);

        if(ui->comboBox->currentText() == "Char"){
            char c = ui->le_itemData->text().at(0).toLatin1();
            charQ.insert(c, ui->sb_priority->value());
        }
        else if(ui->comboBox->currentText() == "Double"){
            doubleQ.insert(ui->le_itemData->text().toDouble(), ui->sb_priority->value());
            val->setText(QString::number(ui->le_itemData->text().toDouble()));\
        }
        else if(ui->comboBox->currentText() == "String"){
            stringQ.insert(ui->le_itemData->text().toStdString(), ui->sb_priority->value());
        }
        else{
            intQ.insert(ui->le_itemData->text().toInt(), ui->sb_priority->value());
        }
        ui->table_items->setItem(0, 0, val);
        ui->table_items->setItem(0, 1, priority);
    }
}

void MainWindow::deleteItem()
{
    int highestP = 0;
    if(ui->comboBox->currentText() == "Char"){
        highestP = charQ.getHighest();
        char c = charQ.deleteItem();
        deleteFromTable(c, highestP);
    }
    else if(ui->comboBox->currentText() == "Double"){
        highestP = doubleQ.getHighest();
        double d = doubleQ.deleteItem();
        deleteFromTable(d, highestP);
    }
    else if(ui->comboBox->currentText() == "String"){
        highestP = stringQ.getHighest();
        string s = stringQ.deleteItem();
        deleteFromTable(s, highestP);
    }
    else{
        highestP = intQ.getHighest();
        int i = intQ.deleteItem();
        deleteFromTable(i, highestP);
    }
}

void MainWindow::deleteFromTable(string &val, int p)
{
    for(int i = 0; i < ui->table_items->rowCount(); ++i){
        if((ui->table_items->item(i, 1)->text().toInt() == p) && (ui->table_items->item(i, 0)->text() == val.c_str())){
            ui->table_items->removeRow(i);
            break;
        }
    }
}

void MainWindow::deleteFromTable(int val, int p)
{
    for(int i = 0; i < ui->table_items->rowCount(); ++i){
        if((ui->table_items->item(i, 1)->text().toInt() == p) && (ui->table_items->item(i, 0)->text() == QString::number(val))){
            ui->table_items->removeRow(i);
            break;
        }
    }
}

void MainWindow::deleteFromTable(char val, int p){
    for(int i = 0; i < ui->table_items->rowCount(); ++i){
        if((ui->table_items->item(i, 1)->text().toInt() == p) && (ui->table_items->item(i, 0)->text().at(0).toLatin1() == val)){
            ui->table_items->removeRow(i);
            break;
        }
    }
}

void MainWindow::deleteFromTable(double val, int p)
{
    for(int i = 0; i < ui->table_items->rowCount(); ++i){
        if((ui->table_items->item(i, 1)->text().toInt() == p) && (ui->table_items->item(i, 0)->text().toDouble() == val)){
            ui->table_items->removeRow(i);
            break;
        }
    }
}

void MainWindow::on_pb_clear_clicked()
{
    ui->table_items->clearContents();
    ui->table_items->setRowCount(0);
    if(ui->comboBox->currentText() == "Char"){
        charQ.clear();
    }
    else if(ui->comboBox->currentText() == "Double"){
        doubleQ.clear();
    }
    else if(ui->comboBox->currentText() == "String"){
        stringQ.clear();
    }
    else{
        intQ.clear();
    }
}
