#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "PQueue.h"
#include <string>
#include <QRegExpValidator>
#include <QTableWidget>
#include <string>
namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_pb_add_clicked();

    void deleteItem();
    void on_pb_clear_clicked();

private:
    void deleteFromTable(std::string& val, int p);
    void deleteFromTable(int val, int p);
    void deleteFromTable(char val, int p);
    void deleteFromTable(double val, int p);
    PQueue<int> intQ;
    PQueue<std::string> stringQ;
    PQueue<char> charQ;
    PQueue<double> doubleQ;
    QRegExpValidator* rgxV;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
