#-------------------------------------------------
#
# Project created by QtCreator 2016-04-21T00:27:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PQueueDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp
INCLUDEPATH += .
HEADERS  += mainwindow.h \
    PQueue.h

FORMS    += mainwindow.ui
CONFIG += c++11
