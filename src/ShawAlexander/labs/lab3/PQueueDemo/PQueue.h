#ifndef PQUEUEDEMO_PQUEUE_H
#define PQUEUEDEMO_PQUEUE_H
#include <vector>
#include <algorithm>

template <typename T> class PQueue {
public:
    PQueue();
    PQueue(PQueue& p);
    ~PQueue();
    inline unsigned int length();
    void insert(const T& item, const int p = 0);
    T deleteItem();
    void clear();
    int getHighest();
protected:
    struct elem{
        T data;
        elem* next = nullptr;
        int priority;
    } *item;
    int highest = 0;
    unsigned int size = 0;
    void updatePriority();
};

template <typename T> PQueue<T>::PQueue()
{
    this->item = nullptr;
}

template <typename T> PQueue<T>::PQueue(PQueue &p)
{
    this->item = nullptr;
    elem* copiedItem = p.item;
    while(copiedItem){
        this->insert(copiedItem->data, copiedItem->priority);
        copiedItem = copiedItem->next;
    }
    highest = p.highest;
}

template <typename T> PQueue<T>::~PQueue()
{
    elem *rmItem = this->item;
    elem *tmp;
    while (rmItem) {
        tmp = rmItem->next;
        delete rmItem;
        rmItem = tmp;
    }
}

template <typename T>void PQueue<T>::insert(const T& item, const int p) {
    if(this->item) {
        elem* newItem = this->item;
        while (newItem->next) {
            newItem = newItem->next;
        }
        newItem->next = new elem();
        newItem->next->data = item;
        newItem->next->priority = p;
        newItem = nullptr;
    }
    else{
        this->item = new elem();
        this->item->data = item;
        this->item->priority = p;
    }
    if(p > highest)
        highest = p;
    ++size;
}

template <typename T>T PQueue<T>::deleteItem()
{
    T removed;
    if(this->item){
        elem *tmp = this->item;
        if(this->item->priority == highest){
            this->item = this->item->next;
            removed = tmp->data;
            delete tmp;
        }
        else{
            while(tmp->next){
                if(tmp->next->priority == highest){
                    elem *rmItem = tmp->next;
                    if(tmp->next->next)
                        tmp->next = tmp->next->next;
                    else
                        tmp->next = nullptr;
                    removed = rmItem->data;
                    delete rmItem;
                    break;
                }
                tmp = tmp->next;
            }
        }
        --size;
        highest = 0;
        this->updatePriority();
    }
    return removed;
}

template <typename T>void PQueue<T>::clear()
{
    if(size){
        elem* tmp = this->item;
        while(tmp){
            this->item = this->item->next;
            delete tmp;
            tmp = this->item;
        }
        highest = size = 0;
    }
}

template <typename T>int PQueue<T>::getHighest()
{
    return highest;
}

template <typename T>void PQueue<T>::updatePriority()
{
    elem* tmp = this->item;
    while(tmp){
        if(highest < tmp->priority)
            highest = tmp->priority;
        tmp = tmp->next;
    }
}

template <typename T>unsigned int PQueue<T>::length() {
    return size;
}
#endif //PQUEUEDEMO_PQUEUE_H
