#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <algorithm>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, this->size(), qApp->desktop()->availableGeometry()));
    QTimer* timer = new QTimer;
    QStandardItemModel* model = new QStandardItemModel;
    model->setColumnCount(6);
    model->setHeaderData(0, Qt::Horizontal, "Category");
    model->setHeaderData(1, Qt::Horizontal, "Name");
    model->setHeaderData(2, Qt::Horizontal, "Start");
    model->setHeaderData(3, Qt::Horizontal, "End");
    model->setHeaderData(4, Qt::Horizontal, "Days left");
    model->setHeaderData(5, Qt::Horizontal, "Progress");
    ui->tableView->setModel(model);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
    timer->setInterval(1000);
    timer->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    //making last column stretch to the right edge
    if(ui->tableView->horizontalHeader()->count())
        ui->tableView->horizontalHeader()->setSectionResizeMode(ui->tableView->horizontalHeader()->count()-1, QHeaderView::Stretch);
}

void MainWindow::updateModel()
{
    QStandardItemModel* model = (QStandardItemModel*) ui->tableView->model();
    model->removeRows(0, model->rowCount());
    for(Event &e : eventsList){
        model->setRowCount(model->rowCount()+1);
        int i = model->rowCount() - 1;
        if(i >= 0){
            model->setItem(i, 0, new QStandardItem(QString(e.getCategory())));
            model->setItem(i, 1, new QStandardItem(QString(e.getTitle())));
            model->setItem(i, 2, new QStandardItem(QString(e.getStart().toString())));
            model->setItem(i, 3, new QStandardItem(QString(e.getEnd().toString())));

                if((QDateTime::currentDateTime()).daysTo(e.getEnd()) >= 0)
                    model->setItem(i, 4, new QStandardItem(QString::number(QDateTime(QDateTime::currentDateTime()).daysTo(e.getEnd()))));
                else
                    model->setItem(i, 4, new QStandardItem(QString("-1")));
        }
    }
    ui->tableView->resizeColumnsToContents();
    ui->tableView->resizeRowsToContents();
    resizeEvent(0);
}

void MainWindow::timeout()
{
    delete ui->tableView->itemDelegateForColumn(5);
    ui->tableView->setItemDelegateForColumn(5, new MyDelegate(ui->tableView));
}

void MainWindow::on_pbLoad_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Select an events file"), tr(""), tr("XML (*.xml)"));
    eventsList.clear();
    QFile events(path);
    events.open(QIODevice::ReadOnly);
    QXmlStreamReader xml(&events);
    QXmlStreamReader::TokenType tokenType;
    QMessageBox b;
    QString category, title;
    QStringList startData, endData;

    while(!xml.atEnd() && !xml.hasError()){
        tokenType = xml.readNext();
        if(tokenType == QXmlStreamReader::StartDocument)
            continue;
        if(tokenType == QXmlStreamReader::StartElement){
            if(xml.name() == "event")
                continue;
            if(xml.name() == "category"){
                xml.readNext();
                category = xml.text().toString();
            }
            else if(xml.name() == "title"){
                xml.readNext();
                title = xml.text().toString();
            }
            else if(xml.name() == "beginDate" || xml.name() == "beginTime"){
                xml.readNext();
                startData.append(xml.text().toString().split('-'));
            }
            else if(xml.name() == "endDate"){
                xml.readNext();
                endData.append(xml.text().toString().split('-'));
            }
            else if(xml.name() == "endTime"){
                xml.readNext();
                endData.append(xml.text().toString().split('-'));
                Event newEvent(category, title);
                newEvent.setStart(startData[0].toInt(), startData[1].toInt(), startData[2].toInt(),
                        startData[3].toInt(), startData[4].toInt(), startData[5].toInt());
                newEvent.setEnd(endData[0].toInt(), endData[1].toInt(), endData[2].toInt(),
                        endData[3].toInt(), endData[4].toInt(), endData[5].toInt());
                eventsList.push_back(newEvent);
                category.clear();
                title.clear();
                startData.clear();
                endData.clear();
            }
        }
    }
    updateModel();
}
