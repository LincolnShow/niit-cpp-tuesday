#ifndef EVENT_H
#define EVENT_H
#include <qdatetime.h>
#include <QString>
class Event
{
public:
    Event();
    Event(QString category, QString title);
    void setStart(int day, int month, int year, int hh = 0, int mm = 0, int ss = 0);
    void setEnd(int day, int month, int year, int hh = 0, int mm = 0, int ss = 0);
    void setCategory(QString s);
    void setTitle(QString s);
    QString getCategory();
    QString getTitle();
    QDateTime getStart();
    QDateTime getEnd();
protected:
    QString category, title;
    QDateTime start, end;
};

#endif // EVENT_H
