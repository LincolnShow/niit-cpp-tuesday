#include "event.h"

Event::Event()
{
    start = end = QDateTime();
}

Event::Event(QString category, QString title)
{
    this->category = category;
    this->title = title;
}

void Event::setStart(int day, int month, int year, int hh, int mm, int ss)
{
    start.setDate(QDate(year, month, day));
    start.setTime(QTime(hh, mm, ss));
}

void Event::setEnd(int day, int month, int year, int hh, int mm, int ss)
{
    end.setDate(QDate(year, month, day));
    end.setTime(QTime(hh, mm, ss));
}

void Event::setCategory(QString s)
{
    category = s;
}

void Event::setTitle(QString s)
{
    title = s;
}

QString Event::getCategory()
{
    return category;
}

QString Event::getTitle()
{
    return title;
}

QDateTime Event::getStart()
{
    return start;
}

QDateTime Event::getEnd()
{
    return end;
}
