#-------------------------------------------------
#
# Project created by QtCreator 2016-04-27T23:59:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QEvents
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    event.cpp
INCLUDEPATH += .
HEADERS  += mainwindow.h \
    event.h \
    mydelegate.h

FORMS    += mainwindow.ui
